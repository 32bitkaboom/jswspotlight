//
//  JSWSpotlightView.h
//  CTFAnimationDemo
//
//  Created by John Watson on 9/14/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSWSpotlightView : UIView

-(void)relayout;
-(void)rotateWithIncrement:(CGFloat)angleIncrement animated:(BOOL)animated;
-(void)rotateToAngle:(CGFloat)angle animated:(BOOL)animated;
-(void)wiggle;

@end
