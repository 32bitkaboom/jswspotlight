//
//  JSWSpotlightView.m
//  CTFAnimationDemo
//
//  Created by John Watson on 9/14/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import "JSWSpotlightView.h"

static const CGFloat JSW_ROTATE_ANIM_DURATION = 0.5;

@interface JSWSpotlightView ()

@property (weak, nonatomic) IBOutlet UIImageView *spotlightSourceImage;
@property (weak, nonatomic) IBOutlet UIImageView *spotlightTriangleImage;
@property (weak, nonatomic) IBOutlet UIImageView *spotlightFloorImage;
@property (weak, nonatomic) IBOutlet UIView *spotlightFloorContainerView;

@property (nonatomic) CGFloat rotationAngle;

//variables for sizing calculations
@property (nonatomic) CGFloat beamHeight;
@property (nonatomic) CGFloat beamWidth;
@property (nonatomic) CGFloat spotlightSourceHalfAngle;
@property (nonatomic) CGFloat floorSpotlightStartingXPos;

@end

@implementation JSWSpotlightView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        // what this does...
        // it initiates the xib and all of its IBOutlets so that they are available to this class it then adds that xib to itself.
        // Since "self" is actually the view in the containing viewController's .xib (or .storyboard),
        // there is an additional superfluous UIView in the view stack.
        // The viewControllers UIView, and the top level UIView for the .xib associated with this class.
        // Therefore, it is necessary to programmatically set the self.backgroundColor to a clear color, and it is also necessary for the
        // top level view in this class's .xib to be invisible since once this code is ran it is no longer accessible
        // (self is the containing class's .xib's UIView, NOT this class's .xib's UIView).
        // Hopefully all this will be fixed as of iOS 8 but for now we have to do this hack.
        UIView *mainView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
        [self addSubview:mainView];
        
        [self relayout];
        self.backgroundColor = [UIColor clearColor];
        mainView.backgroundColor = [UIColor clearColor];
        [self setAnchorPoint:CGPointMake(0.5, 0.0) forView:self.spotlightTriangleImage];
    }
    return self;
}

-(void) relayout
{
    CGFloat midX = self.frame.size.width / 2;
    CGFloat deviceWidth = [self getDeviceWidth];
    
    CGPoint floorContainerOrigin = [self convertPoint:CGPointZero toView:[UIApplication sharedApplication].delegate.window];
    //for some reason the window convert point function doesnt take into account the scale
    CGFloat scale = [[UIScreen mainScreen] scale];
    self.spotlightFloorContainerView.frame = CGRectMake(-floorContainerOrigin.x * scale, //make the floor the width of the screen and put it at screen (0,0) - this guarantees it will be wide enough
                                                        0,
                                                        deviceWidth,
                                                        self.frame.size.height);

    
    CGPoint spotlightOrigin = [self convertPoint:CGPointMake(0, 0) toView:self.spotlightFloorContainerView];
    self.spotlightTriangleImage.frame = CGRectMake(spotlightOrigin.x, //put the spotlight in the middle of our view
                                                   0,
                                                   self.frame.size.width,
                                                   self.frame.size.height + 300); //add 100 so it extends well beyond the bottom of the floor container view
                                                                                  //this allows it to be "cut off" by the floor
    
    self.spotlightSourceImage.center = CGPointMake(midX, 0);
    

//      ^
//     /θ\
//    / | \
//   /  |b \
//  /___|_a_\
//  theta = atan(b/a) where b = height, a = (width/2)
    
    //set calculation variables
    self.spotlightSourceHalfAngle = atanf((self.spotlightTriangleImage.frame.size.width/2) / self.spotlightTriangleImage.frame.size.height);
    self.beamHeight = self.frame.size.height;
    self.rotationAngle = 0.0f;
    self.floorSpotlightStartingXPos = midX;
    [self updateFloorSpotlightFrame];
}

-(void)updateFloorSpotlightFrame
{
    //there are three right triangles involved in this calculation
    CGFloat theta   = self.rotationAngle;
    
    CGFloat b = self.beamHeight * tanf(theta - self.spotlightSourceHalfAngle);
    CGFloat c = self.beamHeight * tanf(self.rotationAngle);
    CGFloat l = c - b;
    CGFloat r = (self.beamHeight * (tanf(theta + self.spotlightSourceHalfAngle))) - c;
    
    CGFloat newBeamWidth = l + r;
    CGFloat newBeamCenter = self.floorSpotlightStartingXPos - (b + (newBeamWidth / 2));
    
    CGRect newFloorSpotlightImageFrame = self.spotlightFloorImage.frame;
    newFloorSpotlightImageFrame.size.width = newBeamWidth;
    self.spotlightFloorImage.frame  = newFloorSpotlightImageFrame;
    self.spotlightFloorImage.center = CGPointMake(newBeamCenter, self.frame.size.height);
}

-(void)rotateWithIncrement:(CGFloat)angleIncrement animated:(BOOL)animated
{
    self.rotationAngle += angleIncrement;
    [self rotateToAngle:self.rotationAngle animated:animated];
}

- (void)rotateToAngle:(CGFloat)angle animated:(BOOL)animated
{
    [self rotateToAngle:angle withDuration:JSW_ROTATE_ANIM_DURATION andDelay:0.0 animated:animated]; //perform rotation with stock duration and no delay
}

- (void)rotateToAngle:(CGFloat)angle withDuration:(CGFloat)duration andDelay:(CGFloat)delay animated:(BOOL)animated
{
    self.rotationAngle = angle;
    
    if( animated )
    {
        [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.spotlightTriangleImage setTransform:CGAffineTransformMakeRotation(self.rotationAngle)];
            [self updateFloorSpotlightFrame];
        } completion:^(BOOL finished) {
            //no-op
        }];
    }
    else
    {
        [self.spotlightTriangleImage setTransform:CGAffineTransformMakeRotation(self.rotationAngle)];
        [self updateFloorSpotlightFrame];
    }
}

- (void) wiggle
{
    CGFloat   rotationAngle = 0.0;
    int min = 0;
    int max = 15;
    NSInteger negPos;  //used to make the number randomly negative or positive
    CGFloat   randNum; //a random value to use to move our rotation angle
    
    NSUInteger wiggleCount = 2;
    for( int x = 0; x <= wiggleCount; x++ )
    {
        negPos = arc4random() % 2 ? 1 : -1; //generate a randomly negative or positive multiplier
        randNum = (float)(arc4random_uniform(max) + min); //generate a random float between min and max
        randNum *= (0.01 * negPos); //bring the randome number down between 0 and 1 and make it randomly negative or positive
        
        if( x == wiggleCount )
        {
            rotationAngle = 0.0; //if we are on the last "wiggle", reset back to 0
        }
        else
        {
            rotationAngle = self.rotationAngle + randNum; //set our desired angle to our currentAngle modified by the random number
//            rotationAngle = randNum; //set our desired angle to the random number
        }
        rotationAngle = x == wiggleCount ? 0.0 : self.rotationAngle + (randNum); //if we are on the last animation, reset back to 0.0 angle
        [self rotateToAngle:rotationAngle withDuration:JSW_ROTATE_ANIM_DURATION andDelay:JSW_ROTATE_ANIM_DURATION * x animated:YES];
    }
}

-(void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view
{
    CGPoint newPoint = CGPointMake(view.bounds.size.width * anchorPoint.x, view.bounds.size.height * anchorPoint.y);
    CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x, view.bounds.size.height * view.layer.anchorPoint.y);
    
    newPoint = CGPointApplyAffineTransform(newPoint, view.transform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, view.transform);
    
    CGPoint position = view.layer.position;
    
    position.x -= oldPoint.x;
    position.x += newPoint.x;
    
    position.y -= oldPoint.y;
    position.y += newPoint.y;
    
    view.layer.position = position;
    view.layer.anchorPoint = anchorPoint;
}

-(CGFloat) getDeviceWidth
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    CGSize size = [UIScreen mainScreen].bounds.size;
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        return size.height;
    }
    
    return size.width;
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self relayout];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self wiggle];
}


@end
