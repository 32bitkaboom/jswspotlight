//
//  AppDelegate.h
//  JSWSpotlight
//
//  Created by John Watson on 9/16/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

