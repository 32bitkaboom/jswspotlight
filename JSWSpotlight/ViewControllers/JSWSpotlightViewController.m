//
//  SpotlightViewController.m
//  CTFAnimationDemo
//
//  Created by John Watson on 9/14/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import "JSWSpotlightViewController.h"
#import "JSWSpotlightView.h"

const CGFloat ROTATION_ANGLE = 0.1;

@interface JSWSpotlightViewController ()

@property (weak, nonatomic) IBOutlet JSWSpotlightView *leftSpotlightView;
@property (weak, nonatomic) IBOutlet JSWSpotlightView *centerSpotlightView;
@property (weak, nonatomic) IBOutlet JSWSpotlightView *rightSpotlightView;
@property (weak, nonatomic) IBOutlet UIView *tripleSpotlightContainerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tripleSpotlightViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tripleSpotlightViewHeightConstraint;
@end

@implementation JSWSpotlightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(wiggle)];
    [self.tripleSpotlightContainerView addGestureRecognizer:tapRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)rotateLeft:(id)sender
{
    [self.leftSpotlightView rotateWithIncrement:ROTATION_ANGLE animated:YES];
    [self.centerSpotlightView rotateWithIncrement:ROTATION_ANGLE animated:YES];
    [self.rightSpotlightView rotateWithIncrement:ROTATION_ANGLE animated:YES];
}

- (IBAction)rotateRight:(id)sender
{
    [self.leftSpotlightView rotateWithIncrement:(ROTATION_ANGLE * -1) animated:YES];
    [self.centerSpotlightView rotateWithIncrement:(ROTATION_ANGLE * -1) animated:YES];
    [self.rightSpotlightView rotateWithIncrement:(ROTATION_ANGLE * -1) animated:YES];
}

- (IBAction)reset:(id)sender
{
    [self.leftSpotlightView rotateToAngle:0.0 animated:YES];
    [self.centerSpotlightView rotateToAngle:0.0 animated:YES];
    [self.rightSpotlightView rotateToAngle:0.0 animated:YES];
}

- (void) wiggle
{
    [self.leftSpotlightView wiggle];
    [self.centerSpotlightView wiggle];
    [self.rightSpotlightView wiggle];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone )
    {
        if( UIInterfaceOrientationIsPortrait(toInterfaceOrientation) )
        {
            NSLog(@"Rotating to portrait...");
            self.tripleSpotlightViewWidthConstraint.constant = 210;
            self.tripleSpotlightViewHeightConstraint.constant = 357;
            [self.view layoutIfNeeded];
        }
        else
        {
            NSLog(@"Rotating to landscape...");
            self.tripleSpotlightViewWidthConstraint.constant =357;
            self.tripleSpotlightViewHeightConstraint.constant = 210;
            [self.view layoutIfNeeded];
        }
        
        [self.leftSpotlightView relayout];
        [self.centerSpotlightView relayout];
        [self.rightSpotlightView relayout];
    }
}

@end
